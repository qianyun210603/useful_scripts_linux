import subprocess
import sys
import os
import os.path
import ctypes
import re
import time

rootdir = '/root/Downloads'

def getFileList(path):
    cdres = subprocess.run(['BaiduPCS-Go', 'cd', path], stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    print(cdres.stdout.decode())
    print('改变工作目录: '+path)
    if not '改变工作目录: '+path in cdres.stdout.decode():
        raise RuntimeError('cannot switch to folder')
    lsres = subprocess.run(['BaiduPCS-Go', 'ls'], stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    for i in range(10):
        if '当前目录:' in lsres.stdout.decode():
            fl = lsres.stdout.decode().split('\n')[4:]
            pattern = re.compile(r' *\d+ *(-|\d*\.?\d*[TGMK]?B) *\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2} *(.*)')
            files = []
            for item in fl:
                if '文件总数' in item and '目录总数' in item:
                    break
                name = re.fullmatch(pattern, item).group(2).rstrip(' ')
                if not name.endswith('/'):
                    files.append(name)
            print(files)
            return files
        print('attemp {} failed'.format(i))
        time.sleep(10)
        lsres = subprocess.run(['BaiduPCS-Go', 'ls'], stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    raise RuntimeError('cannot get file list')

def download(fileList, path = None):
    with open('/root/downloadrecord.txt', 'a+') as record, open('/root/errlog.txt', 'w+') as errlog:
        for f in fileList:
            for i in range(10):
                res = subprocess.run(['BaiduPCS-Go', 'd', '-saveto', rootdir, f], stdout = subprocess.PIPE, stderr = subprocess.PIPE)
                if '下载完成, 保存位置' in res.stdout.decode():
                    rarname = 'file{}'.format(ctypes.c_size_t(hash(f)).value)
                    subprocess.run(['rar', 'a', os.path.join('/var/www/html', rarname), os.path.join(rootdir, f), '-hpcyq06284015', '-m0', '-ep1'], stdout = subprocess.PIPE, stderr = subprocess.PIPE)
                    record.write('{}\t{}\n'.format(f, rarname))
                    os.remove(os.path.join(rootdir, f))
                    if not '检验文件有效性成功' in res.stdout.decode():
                        errlog.write('{} verification failed'.format(f))
                    print('Download and compress succeed: {}'.format(f))
                    time.sleep(5)
                    break
                else:
                    if i < 10:
                        print('Retry download {} after 5 minutes'.format(f))
                        time.sleep(300)
                    else:
                        errlog.write('{} failed'.format(f))

if __name__=='__main__':
    paths = sys.argv[1:]
    for path in paths:
        filelist = getFileList(path)
        download(filelist)
