#!/usr/bin/env python3
import sys
import os
import socket
from urllib.parse import urljoin

ipaddr = '139.59.24.27' #socket.gethostbyname(socket.gethostname())
port = ''
rootdir = '/var/www/html/'
extlist = set(['.rar', 'mkv', '.avi', '.mp4', '.wmv', '.jpg', '.jpeg', '.png', '.py'])

urlprefix = 'http://' + ipaddr + ':' + port
for mydir, _, filelist in os.walk(rootdir):
    for file in filelist:
        myfile = os.path.join(mydir, file)[len(rootdir):].replace('\\', '/')
        if (os.path.splitext(myfile)[1] in extlist):
            print(urljoin(urlprefix, myfile))

